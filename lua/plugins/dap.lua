-- TODO: UI extension telescope-dap.nvim cmp-dap nvim-dap-repl-highlights
return {
	"mfussenegger/nvim-dap",
	desc = "Debugging support. Requires language specific adapters to be configured.",
	dependencies = {
		{
			"rcarriga/nvim-dap-ui",
			dependencies = {
				"nvim-neotest/nvim-nio",
			},
			keys = {
				{
					rc.plugin.dap.ui,
					function()
						require("dapui").toggle({})
					end,
					desc = "Dap UI",
				},
				{
					rc.plugin.dap.eval,
					function()
						require("dapui").eval()
					end,
					desc = "Eval",
					mode = { "n", "v" },
				},
			},
			opts = {},
			config = function(_, opts)
				local dap = require("dap")
				local dapui = require("dapui")
				dapui.setup(opts)
				dap.listeners.after.event_initialized["dapui_config"] = function()
					dapui.open({})
				end
				dap.listeners.before.event_terminated["dapui_config"] = function()
					dapui.close({})
				end
				dap.listeners.before.event_exited["dapui_config"] = function()
					dapui.close({})
				end
			end,
		},
		{
			"theHamsta/nvim-dap-virtual-text",
			opts = {},
		},
		{
			"jay-babu/mason-nvim-dap.nvim",
			dependencies = {
				"mason.nvim",
			},
			cmd = { "DapInstall", "DapUninstall" },
			opts = {
				-- Makes a best effort to setup the various debuggers with
				-- reasonable debug configurations
				automatic_installation = true,

				-- You can provide additional configuration to the handlers,
				-- see mason-nvim-dap README for more information
				handlers = {},

				-- You'll need to check that you have the required things installed
				-- online, please don't ask me how to install them :)
				ensure_installed = {
					-- Update this to ensure that you have the debuggers for the langs you want
				},
			},
		},
		{
			"jbyuki/one-small-step-for-vimkind",
			config = function()
				local dap = require("dap")
				dap.adapters.nlua = function(callback, conf)
					local adapter = {
						type = "server",
						host = conf.host or "127.0.0.1",
						port = conf.port or 8086,
					}
					if conf.start_neovim then
						local dap_run = dap.run
						dap.run = function(c)
							adapter.port = c.port
							adapter.host = c.host
						end
						require("osv").run_this()
						dap.run = dap_run
					end
					callback(adapter)
				end
				dap.configurations.lua = {
					{
						type = "nlua",
						request = "attach",
						name = "Run this file",
						start_neovim = {},
					},
					{
						type = "nlua",
						request = "attach",
						name = "Attach to running Neovim instance (port = 8086)",
						port = 8086,
					},
				}
			end,
		},
	},
	keys = {
		{
			rc.plugin.dap.breakpoint_set_condition,
			function()
				require("dap").set_breakpoint(vim.fn.input("Breakpoint condition: "))
			end,
			desc = "Breakpoint Condition",
		},
		{
			rc.plugin.dap.breakpoint_toggle,
			function()
				require("dap").toggle_breakpoint()
			end,
			desc = "Toggle Breakpoint",
		},
		{
			rc.plugin.dap.continue,
			function()
				require("dap").continue()
			end,
			desc = "Continue",
		},
		{
			rc.plugin.dap.run_to_cursor,
			function()
				require("dap").run_to_cursor()
			end,
			desc = "Run to Cursor",
		},
		{
			rc.plugin.dap.goto_line,
			function()
				require("dap").goto_()
			end,
			desc = "Go to Line (No Execute)",
		},
		{
			rc.plugin.dap.step_into,
			function()
				require("dap").step_into()
			end,
			desc = "Step Into",
		},
		{
			rc.plugin.dap.down,
			function()
				require("dap").down()
			end,
			desc = "Down",
		},
		{
			rc.plugin.dap.up,
			function()
				require("dap").up()
			end,
			desc = "Up",
		},
		{
			rc.plugin.dap.run_last,
			function()
				require("dap").run_last()
			end,
			desc = "Run Last",
		},
		{
			rc.plugin.dap.step_out,
			function()
				require("dap").step_out()
			end,
			desc = "Step Out",
		},
		{
			rc.plugin.dap.step_over,
			function()
				require("dap").step_over()
			end,
			desc = "Step Over",
		},
		{
			rc.plugin.dap.pause,
			function()
				require("dap").pause()
			end,
			desc = "Pause",
		},
		{
			rc.plugin.dap.repl,
			function()
				require("dap").repl.toggle()
			end,
			desc = "Toggle REPL",
		},
		{
			rc.plugin.dap.session,
			function()
				require("dap").session()
			end,
			desc = "Session",
		},
		{
			rc.plugin.dap.terminate,
			function()
				require("dap").terminate()
			end,
			desc = "Terminate",
		},
		{
			rc.plugin.dap.widgets_hover,
			function()
				require("dap.ui.widgets").hover()
			end,
			desc = "Widgets Hover",
		},
	},
	config = function()
		vim.api.nvim_set_hl(0, "DapStoppedLine", { default = true, link = "Visual" })

		for name, sign in pairs(rc.icons.dap) do
			sign = type(sign) == "table" and sign or { sign }
			vim.fn.sign_define("Dap" .. name, {
				text = sign[1],
				texthl = sign[2] or "DiagnosticInfo",
				linehl = sign[3],
				numhl = sign[3],
			})
		end

		-- setup dap config by vscode launch.json file
		local vscode = require("dap.ext.vscode")
		local ft = require("mason-nvim-dap.mappings.filetypes")
		local filetypes = vim.tbl_deep_extend("force", ft, {
			["node"] = { "javascriptreact", "typescriptreact", "typescript", "javascript" },
			["pwa-node"] = { "javascriptreact", "typescriptreact", "typescript", "javascript" },
		})
		local json = require("plenary.json")
		vscode.json_decode = function(str)
			return vim.json.decode(json.json_strip_comments(str))
		end
		vscode.load_launchjs(nil, filetypes)
	end,
}

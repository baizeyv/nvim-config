-- TODO: modify the lualine and edgy
return {
	{
		"folke/trouble.nvim",
		keys = {
			{
				rc.plugin.trouble.diagnostics_all,
				"<CMD>Trouble diagnostics toggle<CR>",
				desc = "Diagnostics (Trouble)",
			},
			{
				rc.plugin.trouble.diagnostics_buffer,
				"<CMD>Trouble diagnostics toggle filter.buf=0<CR>",
				desc = "Buffer Diagnostics (Trouble)",
			},
			{
				rc.plugin.trouble.lsp,
				"<CMD>Trouble lsp toggle focus=false win.position=right<CR>",
				desc = "Lsp references/definitions/... (Trouble)",
			},
			{ rc.plugin.trouble.loclist, "<CMD>Trouble loclist toggle<CR>", desc = "Location List (Trouble)" },
			{ rc.plugin.trouble.qflist, "<CMD>Trouble qflist toggle<CR>", desc = "Quickfix List (Trouble)" },
			{
				rc.plugin.trouble.trouble_prev,
				function()
					if require("trouble").is_open() then
						require("trouble").previous({ skip_groups = true, jump = true })
					else
						local ok, err = pcall(vim.cmd.cprev)
						if not ok then
							vim.notify(err, vim.log.levels.ERROR)
						end
					end
				end,
				desc = "Previous Trouble/Quickfix Item",
			},
			{
				rc.plugin.trouble.trouble_next,
				function()
					if require("trouble").is_open() then
						require("trouble").next({ skip_groups = true, jump = true })
					else
						local ok, err = pcall(vim.cmd.cprev)
						if not ok then
							vim.notify(err, vim.log.levels.ERROR)
						end
					end
				end,
				desc = "Next Trouble/Quickfix Item",
			},
		},
		cmd = {
			"TroubleToggle",
			"Trouble",
		},
		dependencies = {
			"nvim-web-devicons",
		},
		opts = {
			auto_close = false, -- auto close when there are no items
			auto_open = false, -- auto open when there are items
			auto_preview = true, -- automatically open preview when on an item
			auto_refresh = true, -- auto refresh when open
			auto_jump = false, -- auto jump to the item when there's only one
			focus = true, -- Focus the window when opened
			restore = true, -- restores the last location in the list when opening
			follow = true, -- Follow the current item
			indent_guides = true, -- show indent guides
			max_items = 200, -- limit number of items that can be displayed per section
			multiline = true, -- render multi-line messages
			pinned = false, -- When pinned, the opened trouble window will be bound to the current buffer
			---@type trouble.Window.opts
			win = {}, -- window options for the results window. Can be a split or a floating window.
			-- Window options for the preview window. Can be a split, floating window,
			-- or `main` to show the preview in the main editor window.
			---@type trouble.Window.opts
			preview = {
				type = "float",
				relative = "editor",
				border = "rounded",
				title = "Preview",
				title_pos = "center",
				position = { 0, -2 },
				size = { width = 0.4, height = 0.6 },
				zindex = 200,
			},
			-- Throttle/Debounce settings. Should usually not be changed.
			---@type table<string, number|{ms:number, debounce?:boolean}>
			throttle = {
				refresh = 20, -- fetches new data when needed
				update = 10, -- updates the window
				render = 10, -- renders the window
				follow = 10, -- follows the current item
				preview = { ms = 100, debounce = true }, -- shows the preview for the current item
			},
			-- Key mappings can be set to the name of a builtin action,
			-- or you can define your own custom action.
			---@type table<string, string|trouble.Action>
			keys = {
				["?"] = "help",
				r = "refresh",
				R = "toggle_refresh",
				q = "close",
				o = "jump_close",
				["<esc>"] = "cancel",
				["<cr>"] = "jump",
				["<2-leftmouse>"] = "jump",
				["<c-h>"] = "jump_split",
				["<c-v>"] = "jump_vsplit",
				-- go down to next item (accepts count)
				-- j = "next",
				e = "next",
				["]]"] = "next",
				-- go up to prev item (accepts count)
				-- k = "prev",
				u = "prev",
				["[["] = "prev",
				j = "inspect",
				p = "preview",
				P = "toggle_preview",
				zo = "fold_open",
				zO = "fold_open_recursive",
				zc = "fold_close",
				zC = "fold_close_recursive",
				za = "fold_toggle",
				zA = "fold_toggle_recursive",
				zm = "fold_more",
				zM = "fold_close_all",
				zr = "fold_reduce",
				zR = "fold_open_all",
				zx = "fold_update",
				zX = "fold_update_all",
				zn = "fold_disable",
				zN = "fold_enable",
				zi = "fold_toggle_enable",
			},
			---@type table<string, trouble.Mode>
			modes = {
				symbols = {
					desc = "document symbols",
					mode = "lsp_document_symbols",
					focus = false,
					win = { position = "right" },
					filter = {
						-- remove Package since luals uses it for control flow structures
						["not"] = { ft = "lua", kind = "Package" },
						any = {
							-- all symbol kinds for help / markdown files
							ft = { "help", "markdown" },
							-- default set of symbol kinds
							kind = {
								"Class",
								"Constructor",
								"Enum",
								"Field",
								"Function",
								"Interface",
								"Method",
								"Module",
								"Namespace",
								"Package",
								"Property",
								"Struct",
								"Trait",
							},
						},
					},
				},
			},
            -- stylua: ignore
            icons = {
                ---@type trouble.Indent.symbols
                indent = {
                    top           = "│ ",
                    middle        = "├╴",
                    last          = "└╴",
                    -- last          = "-╴",
                    -- last       = "╰╴", -- rounded
                    fold_open     = " ",
                    fold_closed   = " ",
                    ws            = "  ",
                },
                folder_closed   = " ",
                folder_open     = " ",
                kinds = {
                    Array         = " ",
                    Boolean       = "󰨙 ",
                    Class         = " ",
                    Constant      = "󰏿 ",
                    Constructor   = " ",
                    Enum          = " ",
                    EnumMember    = " ",
                    Event         = " ",
                    Field         = " ",
                    File          = " ",
                    Function      = "󰊕 ",
                    Interface     = " ",
                    Key           = " ",
                    Method        = "󰊕 ",
                    Module        = " ",
                    Namespace     = "󰦮 ",
                    Null          = " ",
                    Number        = "󰎠 ",
                    Object        = " ",
                    Operator      = " ",
                    Package       = " ",
                    Property      = " ",
                    String        = " ",
                    Struct        = "󰆼 ",
                    TypeParameter = " ",
                    Variable      = "󰀫 ",
                },
            },
		},
	},
}

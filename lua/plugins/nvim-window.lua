return {
	"yorickpeterse/nvim-window",
	keys = {
		{
			rc.plugin.window_picker.pick,
			function()
				require("nvim-window").pick()
			end,
			desc = "Jump to window",
		},
	},
	opts = {
		-- The characters available for hinting windows.
		chars = {
			"t",
			"n",
			"s",
			"e",
			"r",
			"i",
			"a",
			"o",
			"d",
			"h",
			"q",
			"l",
			"w",
			"u",
			"p",
			"y",
			"g",
			"j",
			"z",
			"k",
			"x",
			"m",
			"c",
			"v",
			"b",
		},

		-- A group to use for overwriting the Normal highlight group in the floating
		-- window. This can be used to change the background color.
		normal_hl = "Normal",

		-- The highlight group to apply to the line that contains the hint characters.
		-- This is used to make them stand out more.
		hint_hl = "Bold",

		-- The border style to use for the floating window.
		border = "single",

		-- How the hints should be rendered. The possible values are:
		--
		-- - "float" (default): renders the hints using floating windows
		-- - "status": renders the hints to a string and calls `redrawstatus`,
		--   allowing you to show the hints in a status or winbar line
		render = "float",
	},
}

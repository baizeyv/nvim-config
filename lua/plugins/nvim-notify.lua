-- TODO: progress update
return {
	"rcarriga/nvim-notify",
	keys = {
		{
			rc.plugin.notify.dismiss,
			function()
				require("notify").dismiss({ silent = true, pending = true })
			end,
			desc = "Dismiss all notifications",
		},
	},
	opts = {
		level = vim.log.levels.INFO,
		timeout = 0,
		stages = "fade_in_slide_out",
		render = "default",
		background_colour = "NotifyBackground",
		minimum_width = 50,
		max_height = function()
			return math.floor(vim.o.lines * 0.75)
		end,
		max_width = function()
			return math.floor(vim.o.columns * 0.75)
		end,
		on_open = function(win)
			vim.api.nvim_win_set_config(win, { zindex = 100 })
		end,
		on_close = nil,
		fps = 120,
		top_down = true,
		time_formats = {
			notification_history = "%FT%T",
			notification = "%T",
		},
		icons = {
			ERROR = "",
			WARN = "",
			INFO = "",
			DEBUG = "",
			TRACE = "✎",
		},
	},
	init = function()
		local ok = pcall(require, "noice")
		if not ok then
			vim.api.nvim_create_autocmd("User", {
				pattern = "VeryLazy",
				callback = function()
					vim.notify = require("notify")
				end,
			})
		end
	end,
}

local highlight = {
	"RainbowYellow",
	"RainbowBlue",
	"RainbowOrange",
	"RainbowCyan",
	"RainbowGreen",
	"RainbowViolet",
	"RainbowRed",
}

local rainbow_delimiters_opts = {
	query = {
		[""] = "rainbow-delimiters",
		-- lua = 'rainbow-blocks',
	},
	priority = {
		[""] = 110,
		lua = 210,
	},
	highlight = highlight,
}

return {
	"lukas-reineke/indent-blankline.nvim",
	event = "User FILE-SETUP",
	dependencies = {
		"HiPhish/rainbow-delimiters.nvim",
		"mini.indentscope",
	},
	opts = function()
		local white_highlight = {
			"CursorColumn",
			"Whitespace",
		}

		local scope_highlight = {
			"MiniIndentscopeSymbol",
		}

		local hooks = require("ibl.hooks")

		-- create the highlight groups in the highlight setup hook, so they are reset
		-- every time the colorscheme changes
		hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
			vim.api.nvim_set_hl(0, "RainbowRed", { fg = "#E06C75" })
			vim.api.nvim_set_hl(0, "RainbowYellow", { fg = "#E5C07B" })
			vim.api.nvim_set_hl(0, "RainbowBlue", { fg = "#61AFEF" })
			vim.api.nvim_set_hl(0, "RainbowOrange", { fg = "#D19A66" })
			vim.api.nvim_set_hl(0, "RainbowGreen", { fg = "#98C379" })
			vim.api.nvim_set_hl(0, "RainbowViolet", { fg = "#C678DD" })
			vim.api.nvim_set_hl(0, "RainbowCyan", { fg = "#56B6C2" })
			vim.api.nvim_set_hl(0, "MiniIndentscopeSymbol", { fg = "#ced4da" })
		end)

		return {
			scope = { highlight = scope_highlight },
			indent = {
				char = "╎",
				highlight = highlight,
			},
			whitespace = {
				highlight = white_highlight,
				remove_blankline_trail = false,
			},
		}
	end,
	config = function(_, opts)
		local hooks = require("ibl.hooks")
		hooks.register(hooks.type.SCOPE_HIGHLIGHT, hooks.builtin.scope_highlight_from_extmark)
		require("ibl").setup(opts)
		require("rainbow-delimiters.setup").setup(rainbow_delimiters_opts)
	end,
}

-- 󰰪 󰯱 󰰡 󰰞 󰰕 󰙱 󰫰    󰘳     󰷂     󱋚 󱈙 󱍢 󱎙 '' 󱥰 󰴈      󰊖 󰺵 󰊗    󰏮 󰏬 󱑺 󰹡  󰊴 󰖺      󱗃 󱥟  󱑶 󰡾 󰲉 󰘤  󰌌  󰌓  󰘵   󰅟 󰐳 󱌱 󱌲  󰭯       󰒊  󰝺  󰻠     󰽱 󰽷 󰽪 
-- 󰳽 󰞷 󰕷 󰷂 󰭯    
-- TODO: DAP NOICE etc... color ...

local colors = {
	rosewater = "#f4dbd6",
	flamingo = "#f0c6c6",
	pink = "#f5bde6",
	mauve = "#c6a0f6",
	red = "#ed8796",
	maroon = "#ee99a0",
	peach = "#f5a97f",
	yellow = "#eed49f",
	green = "#a6da95",
	teal = "#8bd5ca",
	sky = "#91d7e3",
	sapphire = "#7dc4e4",
	blue = "#8aadf4",
	lavender = "#b7bdf8",
	text = "#cad3f5",
	subtext1 = "#b8c0e0",
	subtext0 = "#a5adcb",
	overlay2 = "#939ab7",
	overlay1 = "#8087a2",
	overlay0 = "#6e738d",
	surface2 = "#5b6078",
	surface1 = "#494d64",
	surface0 = "#363a4f",
	base = "#24273a",
	mantle = "#1e2030",
	crust = "#181926",
}

local icons = {
	NORMAL = "",
	INSERT = "󰷂",
	VISUAL = "󰳽",
	REPLACE = "",
	COMMAND = "󰞷",
	INACTIVE = "",
}

local conditions = {
	buffer_not_empty = function()
		return vim.fn.empty(vim.fn.expand("%:t")) ~= 1
	end,
	hide_in_width = function()
		return vim.fn.winwidth(0) > 80
	end,
	check_git_workspace = function()
		local filepath = vim.fn.expand("%:p:h")
		local gitdir = vim.fn.finddir(".git", filepath .. ";")
		return gitdir and #gitdir > 0 and #gitdir < #filepath
	end,
}

local function get_root_component()
	local opts = {
		cwd = false,
		subdirectory = true,
		parent = true,
		other = true,
		icon = "󱉭",
	}
	local function get()
		local cwd = rc.utils.root.cwd()
		local root = rc.utils.root.get({ normalize = true })
		local name = vim.fs.basename(root)

		if root == cwd then
			-- root is cwd
			return opts.cwd and name
		elseif root:find(cwd, 1, true) == 1 then
			-- root is subdirectory of cwd
			return opts.subdirectory and name
		elseif cwd:find(root, 1, true) == 1 then
			-- root is parent directory of cwd
			return opts.parent and name
		else
			-- root and cwd are not related
			return opts.other and name
		end
	end
	return {
		function()
			return (opts.icon and opts.icon .. " ") .. get()
		end,
		cond = function()
			return type(get()) == "string"
		end,
		color = {
			fg = colors.subtext1,
			gui = "bold",
		},
	}
end

local function format(component, text, hl_group)
	text = text:gsub("%%", "%%%%")
	if not hl_group or hl_group == "" then
		return text
	end
	---@type table<string, string>
	component.hl_cache = component.hl_cache or {}
	local lualine_hl_group = component.hl_cache[hl_group]
	if not lualine_hl_group then
		local utils = require("lualine.utils.utils")
		---@type string[]
		local gui = vim.tbl_filter(function(x)
			return x
		end, {
			utils.extract_highlight_colors(hl_group, "bold") and "bold",
			utils.extract_highlight_colors(hl_group, "italic") and "italic",
		})

		lualine_hl_group = component:create_hl({
			fg = utils.extract_highlight_colors(hl_group, "fg"),
			gui = #gui > 0 and table.concat(gui, ",") or nil,
		}, "LV_" .. hl_group) --[[@as string]]
		component.hl_cache[hl_group] = lualine_hl_group
	end
	return component:format_hl(lualine_hl_group) .. text .. component:get_default_hl()
end

local function get_pretty_path()
	local opts = {
		relative = "cwd",
		modified_hl = "MatchParen",
		directory_hl = "",
		filename_hl = "Bold",
		modified_sign = "",
		length = 3,
	}

	return function(self)
		local path = vim.fn.expand("%:p")
		if path == "" then
			return ""
		end
		local root = rc.utils.root.get({ normalize = true })
		local cwd = rc.utils.root.cwd()

		if opts.relative == "cwd" and path:find(cwd, 1, true) == 1 then
			path = path:sub(#cwd + 2)
		else
			path = path:sub(#root + 2)
		end

		local sep = package.config:sub(1, 1)
		local parts = vim.split(path, "[\\/]")

		if opts.length == 0 then
			parts = parts
		elseif #parts > opts.length then
			parts = { parts[1], "…", table.concat({ unpack(parts, #parts - opts.length + 2, #parts) }, sep) }
		end

		if opts.modified_hl and vim.bo.modified then
			parts[#parts] = parts[#parts] .. opts.modified_sign
			parts[#parts] = format(self, parts[#parts], opts.modified_hl)
		else
			parts[#parts] = format(self, parts[#parts], opts.filename_hl)
		end

		local dir = ""
		if #parts > 1 then
			dir = table.concat({ unpack(parts, 1, #parts - 1) }, sep)
			dir = format(self, dir .. sep, opts.directory_hl)
		end
		return dir .. parts[#parts]
	end
end

return {
	"nvim-lualine/lualine.nvim",
	event = "User PLUGIN-SETUP",
	init = function()
		vim.g.lualine_laststatus = vim.o.statusline
		if vim.fn.argc(-1) > 0 then
			-- set an empty statusline till lualine loads
			vim.o.statusline = " "
		else
			-- hide the statusline on the starter page
			vim.o.laststatus = 0
		end
	end,
	config = function()
		local lualine = require("lualine")

		local cfg = {
			options = {
				-- disable sections and component separators
				component_separators = "",
				section_separators = "",
				theme = {
					-- we are going to use lualine_c and lualine_x as left and right section.
					-- both are highlighted by c theme.
					-- so we are just setting default looks o statusline
					normal = {
						c = {
							fg = colors.fg,
							bg = colors.bg,
						},
					},
					inactive = {
						c = {
							fg = colors.fg,
							bg = colors.bg,
						},
					},
				},
			},
			sections = {
				-- these are to remove the defaults
				lualine_a = {},
				lualine_b = {},
				lualine_y = {},
				lualine_z = {},
				-- these will be filled later
				lualine_c = {},
				lualine_x = {},
			},
			inactive_sections = {
				-- these are to remove the defaults
				lualine_a = {},
				lualine_b = {},
				lualine_c = {},
				lualine_x = {},
				lualine_y = {},
				lualine_z = {},
			},
		}

		--- inserts a component in lualine_c at left section
		local function ins_left(component)
			table.insert(cfg.sections.lualine_c, component)
		end

		--- inserts a component in lualine_x at right section
		local function ins_right(component)
			table.insert(cfg.sections.lualine_x, component)
		end

		ins_left({
			function()
				return "▊"
			end,
			color = { fg = colors.blue }, -- sets highlighting of component
			padding = { left = 0, right = 1 }, -- we don't need space before this
		})

		ins_left({
			-- mode component
			function()
				-- TODO:
				local mode = require("lualine.utils.mode").get_mode()
				return " 󰷂 "
				-- return " " .. (icons[mode] or "") .. " "
			end,
			color = function()
				-- auto change color according to neovims mode
				local mode_color = {
					n = colors.blue,
					i = colors.green,
					v = colors.red,
					[""] = colors.blue,
					V = colors.blue,
					c = colors.magenta,
					no = colors.red,
					s = colors.orange,
					S = colors.orange,
					[""] = colors.orange,
					ic = colors.yellow,
					R = colors.violet,
					Rv = colors.violet,
					cv = colors.red,
					ce = colors.red,
					r = colors.cyan,
					rm = colors.cyan,
					["r?"] = colors.cyan,
					["!"] = colors.red,
					t = colors.red,
				}
				return { fg = mode_color[vim.fn.mode()] }
			end,
			padding = { right = 0, left = 0 },
		})

		ins_left({
			function()
				return "▊"
			end,
			color = { fg = colors.blue }, -- sets highlighting of component
			padding = { left = 1, right = 1 }, -- we don't need space before this
		})

		ins_left({
			"branch",
			icon = "",
			color = {
				fg = colors.crust,
				gui = "bold",
			},
		})

		ins_left({
			"diff",
			symbols = {
				added = " ",
				modified = "󰝤 ",
				removed = " ",
			},
			diff_color = {
				added = {
					fg = colors.green,
				},
				modified = {
					fg = colors.yellow,
				},
				removed = {
					fg = colors.red,
				},
			},
			cond = conditions.hide_in_width,
			padding = { right = 1 },
		})

		ins_left(get_root_component())

		ins_left(get_pretty_path())

		ins_left({
			function()
				return "%="
			end,
		})

		ins_left({
			-- Lsp Server Name
			function()
				local msg = "No Active Lsp"
				local buf_ft = vim.api.nvim_buf_get_option(0, "filetype")
				local clients = rc.utils.lsp.get_clients()
				if next(clients) == nil then
					return msg
				end
				for _, client in ipairs(clients) do
					local filetypes = client.config.filetypes
					if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
						return string.upper(client.name)
					end
				end
				return msg
			end,
			icon = " LSP:",
			color = {
				fg = "#ffffff",
				gui = "bold",
			},
		})

		ins_right({
			"diagnostics",
		})

		ins_right({
			require("lazy.status").updates,
			cond = require("lazy.status").has_updates,
			color = {
				fg = colors.sapphire,
			},
		})

		ins_right({
			"filesize",
			cond = conditions.buffer_not_empty,
		})

		ins_right({
			"o:encoding",
			fmt = string.upper,
			cond = conditions.hide_in_width,
			color = {
				fg = colors.green,
				gui = "bold",
			},
		})

		ins_right({
			"fileformat",
			fmt = string.upper,
			icons_enabled = false,
			color = {
				fg = colors.green,
				gui = "bold",
			},
		})

		ins_right({
			"filetype",
			color = {
				gui = "bold",
			},
			padding = {
				left = 1,
				right = 1,
			},
		})

		ins_right({
			"progress",
			color = {
				fg = colors.text,
				gui = "bold",
			},
			padding = {
				left = 1,
				right = 1,
			},
		})

		ins_right({
			function()
				return ""
			end,
			color = { fg = colors.blue }, -- sets highlighting of component
			padding = { left = 1 }, -- we don't need space before this
		})

		ins_right({
			function()
				local function center_align(s, width)
					local len = #s
					if len >= width then
						return s
					end

					local left_padding = math.floor((width - len) / 2)
					local right_padding = width - len - left_padding
					return string.rep(" ", left_padding) .. s .. string.rep(" ", right_padding)
				end
				local line = vim.fn.line(".")
				local col = vim.fn.virtcol(".")
				local str = string.format("%d:%d", line, col)
				return center_align(str, #str)
			end,
			color = {
				fg = colors.sky,
				gui = "bold",
			},
			padding = {
				left = 1,
				right = 0,
			},
		})

		ins_right({
			function()
				return "▊"
			end,
			color = { fg = colors.blue }, -- sets highlighting of component
			padding = { left = 1 }, -- we don't need space before this
		})

		ins_right({
			function()
				return " " .. os.date("%R")
			end,
			padding = {
				left = 1,
				right = 0,
			},
		})

		ins_right({
			function()
				return "▊"
			end,
			color = { fg = colors.blue }, -- sets highlighting of component
			padding = { left = 1 }, -- we don't need space before this
		})

		lualine.setup(cfg)
	end,
}

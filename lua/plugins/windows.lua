return {
	"anuvyklack/windows.nvim",
	keys = {
		{
			rc.plugin.windows.max,
			function()
				vim.cmd([[WindowsMaximize]])
			end,
		},
		{
			rc.plugin.windows.max_vertically,
			function()
				vim.cmd([[WindowsMaximizeVertically]])
			end,
		},
		{
			rc.plugin.windows.max_horizontally,
			function()
				vim.cmd([[WindowsMaximizeHorizontally]])
			end,
		},
		{
			rc.plugin.windows.equal,
			function()
				vim.cmd([[WindowsEqualize]])
			end,
		},
	},
	dependencies = {
		"anuvyklack/middleclass",
		"anuvyklack/animation.nvim",
	},
	config = function()
		vim.o.winwidth = 10
		vim.o.winminwidth = 10
		vim.o.equalalways = false
		require("windows").setup()
	end,
}

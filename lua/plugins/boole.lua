return {
    "nat-418/boole.nvim",
    event = "User FILE-SETUP",
    opts = {
        mappings = {
            increment = rc.plugin.boole.increment,
            decrement = rc.plugin.boole.decrement,
        },
        -- User defined loops
        additions = {
            { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" },
        },
        allow_caps_additions = {
            { "true", "false" },
            { "yes", "no" },
            { "enable", "disable" },
            -- for example:
            -- enable -> disable
            -- Enable -> Disable
            -- ENABLE -> DISABLE
        },
    },
}

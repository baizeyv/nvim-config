return {
	"echasnovski/mini.surround",
	keys = {
		{ rc.plugin.surround.add, desc = "Add Surround" },
		{ rc.plugin.surround.delete, desc = "Delete Surround" },
		{ rc.plugin.surround.find, desc = "Find Next Surround" },
		{ rc.plugin.surround.find_left, desc = "Find Prev Surround" },
		{ rc.plugin.surround.highlight, desc = "Find Highlight Surround" },
		{ rc.plugin.surround.replace, desc = "Replace Surround" },
	},
	opts = {
		-- Add custom surroundings to be used on top of builtin ones. For more
		-- information with examples, see `:h MiniSurround.config`.
		custom_surroundings = nil,

		-- Duration (in ms) of highlight when calling `MiniSurround.highlight()`
		highlight_duration = 500,

		-- Module mappings. Use `''` (empty string) to disable one.
		mappings = {
			add = rc.plugin.surround.add, -- Add surrounding in Normal and Visual modes
			delete = rc.plugin.surround.delete, -- Delete surrounding
			find = rc.plugin.surround.find, -- Find surrounding (to the right)
			find_left = rc.plugin.surround.find_left, -- Find surrounding (to the left)
			highlight = rc.plugin.surround.highlight, -- Highlight surrounding
			replace = rc.plugin.surround.replace, -- Replace surrounding
			update_n_lines = rc.plugin.surround.update_n_lines, -- Update `n_lines`

			suffix_last = rc.plugin.surround.suffix_last, -- Suffix to search with "prev" method e.g.: <Leader>sdl"
			suffix_next = rc.plugin.surround.suffix_next, -- Suffix to search with "next" method e.g.: <Leader>sdn"
		},

		-- Number of lines within which surrounding is searched
		n_lines = 20,

		-- Whether to respect selection type:
		-- - Place surroundings on separate lines in linewise mode.
		-- - Place surroundings on each line in blockwise mode.
		respect_selection_type = false,

		-- How to search for surrounding (first inside current line, then inside
		-- neighborhood). One of 'cover', 'cover_or_next', 'cover_or_prev',
		-- 'cover_or_nearest', 'next', 'prev', 'nearest'. For more details,
		-- see `:h MiniSurround.config`.
		search_method = "cover",

		-- Whether to disable showing non-error feedback
		silent = false,
	},
}

local parser_path = vim.fn.stdpath("data") .. "/treesitter-parser"
vim.opt.runtimepath:append(parser_path)

return {
	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		event = { "User FILE-SETUP", "User PLUGIN-SETUP" },
		cmd = {
			"TSUpdateSync",
			"TSUpdate",
			"TSInstall",
		},
		dependencies = {
			"nvim-treesitter/nvim-treesitter-textobjects",
		},
		opts = {
			-- A list of parser names, or "all" (the five listed parsers should always be installed)
			ensure_installed = "all",

			-- Install parsers synchronously (only applied to `ensure_installed`)
			sync_install = false,

			-- Automatically install missing parsers when entering buffer
			-- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
			auto_install = true,

			-- List of parsers to ignore installing (or "all")
			ignore_install = { "ocaml", "ocaml_interface" },

			parser_install_dir = parser_path,
			---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
			-- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

			highlight = {
				enable = true,

				-- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
				-- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
				-- the name of the parser)

				-- list of language that will be disabled
				-- disable = {},
				-- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files
				disable = function(lang, buf)
					local max_filesize = 100 * 1024 -- 100 KB
					local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
					if ok and stats and stats.size > max_filesize then
						return true
					end
				end,

				-- Setting this to true will run `:h syntax` and tree-sitter at the same time.
				-- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
				-- Using this option may slow down your editor, and you may see some duplicate highlights.
				-- Instead of true it can also be a list of languages
				additional_vim_regex_highlighting = false,
			},
			indent = {
				enable = true,
			},
			incremental_selection = {
				enable = true,
				keymaps = {
					init_selection = rc.plugin.treesitter.init_selection,
					node_incremental = rc.plugin.treesitter.node_incremental,
					scope_incremental = rc.plugin.treesitter.scope_incremental,
					node_decremental = rc.plugin.treesitter.node_decremental,
				},
			},
			----------------------------------------------------------------
			-- textobjects
			textobjects = {
				select = {
					enable = true,

					-- Automatically jump forward to textobj, similar to targets.vim
					lookahead = true,

					keymaps = {
						["af"] = "@function.outer", -- for example: vaf | daf | <C-/>af
						["kf"] = "@function.inner", -- for example: vkf | dkf | <C-/>kf
						["ac"] = "@class.outer",
						["kc"] = { query = "@class.inner", desc = "Select inner part of a class region" },
						["kl"] = { query = "@loop.inner", desc = "Select inner part of a loop region" },
						["al"] = { query = "@loop.outer", desc = "Select outer part of a loop region" },
						["as"] = { query = "@scope", query_group = "locals", desc = "Select language scope" },
						["kt"] = { query = "@conditional.inner", desc = "Select inner part of a conditional region" },
						["at"] = { query = "@conditional.outer", desc = "Select outer part of a conditioonal region" },
						["ka"] = { query = "@assignment.inner", desc = "Select inner part of a assignment region" },
						["aa"] = { query = "@assignment.outer", desc = "Select outer part of a assignment region" },
						["kg"] = { query = "@parameter.inner", desc = "Select inner part of a parameter region" },
						["ag"] = { query = "@parameter.outer", desc = "Select outer part of a parameter region" },
						["k" .. rc.plugin.comment.normal] = {
							query = "@comment.inner",
							desc = "Select inner part of a comment region",
						},
						["a" .. rc.plugin.comment.normal] = {
							query = "@comment.outer",
							desc = "Select outer part of a comment region",
						},
					},
					selection_modes = {
						["@comment.outer"] = "V", -- v V <c-v>
						["@scope"] = "<c-v>",
						-- etc.
					},
					include_surrounding_whitespace = false,
				},
				swap = {
					enable = true,
					swap_next = {
						["<Leader>og"] = "@parameter.inner",
					},
					swap_previous = {
						["<Leader>oG"] = "@parameter.inner",
					},
				},
				move = {
					enable = true,
					set_jumps = true,
					goto_next_start = {
						["<Leader>]f"] = { query = "@function.outer", desc = "Goto next function" },
						["<Leader>]c"] = { query = "@class.outer", desc = "Goto next class" },
						["<Leader>]l"] = { query = "@loop.*", desc = "Goto next loop" },
						["<Leader>]s"] = { query = "@scope", query_group = "locals", desc = "Goto next scope" },
						["<Leaedr>]z"] = { query = "@fold", query_group = "folds", desc = "Goto next fold region" },
						["<Leaedr>]g"] = {
							query = "@parameter.inner",
							desc = "Goto next parameter",
						},
					},
					goto_next_end = {
						["<Leader>]F"] = { query = "@function.outer", desc = "Goto next function" },
						["<Leader>]C"] = { query = "@class.outer", desc = "Goto next class" },
						["<Leader>]L"] = { query = "@loop.*", desc = "Goto next loop" },
						["<Leader>]S"] = { query = "@scope", query_group = "locals", desc = "Goto next scope" },
						["<Leaedr>]Z"] = { query = "@fold", query_group = "folds", desc = "Goto next fold region" },
						["<Leaedr>]G"] = {
							query = "@parameter.inner",
							desc = "Goto next parameter",
						},
					},
					goto_previous_start = {
						["<Leader>[f"] = { query = "@function.outer", desc = "Goto previous function" },
						["<Leader>[c"] = { query = "@class.outer", desc = "Goto previous class" },
						["<Leader>[l"] = { query = "@loop.*", desc = "Goto previous loop" },
						["<Leader>[s"] = { query = "@scope", query_group = "locals", desc = "Goto previous scope" },
						["<Leaedr>[z"] = { query = "@fold", query_group = "folds", desc = "Goto previous fold region" },
						["<Leaedr>[g"] = {
							query = "@parameter.inner",
							desc = "Goto previous parameter",
						},
					},
					goto_previous_end = {
						["<Leader>[F"] = { query = "@function.outer", desc = "Goto previous function" },
						["<Leaedr>[C"] = { query = "@class.outer", desc = "Goto previoous class" },
						["<Leader>[L"] = { query = "@loop.*", desc = "Goto previous loop" },
						["<Leader>[S"] = { query = "@scope", query_group = "locals", desc = "Goto previous scope" },
						["<Leaedr>[Z"] = { query = "@fold", query_group = "folds", desc = "Goto previous fold region" },
						["<Leaedr>[G"] = {
							query = "@parameter.inner",
							desc = "Goto previous parameter",
						},
					},
					goto_next = {
						["<Leader>]t"] = { query = "@conditional.outer", desc = "Goto next conditional" },
					},
					goto_previous = {
						["<Leader>[t"] = { query = "@conditional.outer", desc = "Goto previous conditional" },
					},
				},
			},
		},
		config = function(_, opts)
			vim.treesitter.language.register("markdown", "notify")
			vim.treesitter.language.register("markdown", "trouble")
			require("nvim-treesitter.configs").setup(opts)
		end,
	},
	{
		"nvim-treesitter/nvim-treesitter-context",
		event = "User FILE-SETUP",
		opts = {
			enable = true, -- Enable this plugin (Can be enabled/disabled later via commands)
			max_lines = 0, -- How many lines the window should span. Values <= 0 mean no limit.
			min_window_height = 0, -- Minimum editor window height to enable context. Values <= 0 mean no limit.
			line_numbers = true,
			multiline_threshold = 20, -- Maximum number of lines to show for a single context
			trim_scope = "outer", -- Which context lines to discard if `max_lines` is exceeded. Choices: 'inner', 'outer'
			mode = "cursor", -- Line used to calculate context. Choices: 'cursor', 'topline'
			-- Separator between context and content. Should be a single character string, like '-'.
			-- When separator is set, the context will only show up when there are at least 2 lines above cursorline.
			separator = nil,
			zindex = 20, -- The Z-index of the context window
			on_attach = nil, -- (fun(buf: integer): boolean) return false to disable attaching
		},
	},
	{
		"windwp/nvim-ts-autotag",
		event = "User FILE-SETUP",
		opts = {
			opts = {
				-- Defaults
				enable_close = true, -- Auto close tags
				enable_rename = true, -- Auto rename pairs of tags
				enable_close_on_slash = true, -- Auto close on trailing </
			},
			-- Also override individual filetype configs, these take priority.
			-- Empty by default, useful if one of the "opts" global settings
			-- doesn't work well in a specific filetype
			per_filetype = {
				["html"] = {
					enable_close = true,
				},
			},
		},
	},
}

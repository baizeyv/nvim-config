local M = {}

vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Mode           | Norm | Ins | Cmd | Vis | Sel | Opr | Term | Lang |
-- Command        +------+-----+-----+-----+-----+-----+------+------+
-- [nore]map      | yes  |  -  |  -  | yes | yes | yes |  -   |  -   |
-- n[nore]map     | yes  |  -  |  -  |  -  |  -  |  -  |  -   |  -   |
-- [nore]map!     |  -   | yes | yes |  -  |  -  |  -  |  -   |  -   |
-- i[nore]map     |  -   | yes |  -  |  -  |  -  |  -  |  -   |  -   |
-- c[nore]map     |  -   |  -  | yes |  -  |  -  |  -  |  -   |  -   |
-- v[nore]map     |  -   |  -  |  -  | yes | yes |  -  |  -   |  -   |
-- x[nore]map     |  -   |  -  |  -  | yes |  -  |  -  |  -   |  -   |
-- s[nore]map     |  -   |  -  |  -  |  -  | yes |  -  |  -   |  -   |
-- o[nore]map     |  -   |  -  |  -  |  -  |  -  | yes |  -   |  -   |
-- t[nore]map     |  -   |  -  |  -  |  -  |  -  |  -  | yes  |  -   |
-- l[nore]map     |  -   | yes | yes |  -  |  -  |  -  |  -   | yes  |

-- opts -> "<buffer>", "<nowait>", "<silent>", "<script>", "<expr>" and "<unique>"

M.map = function(modes, lhs, rhs, opts)
	if #modes > 0 then
		opts = opts or {}
		opts.silent = opts.silent ~= false
		vim.keymap.set(modes, lhs, rhs, opts)
	end
end

local function set_others()
	local map = M.map
	local k = rc.additional
	map("n", k.reload, function()
		rc.reload_config()
	end)
end

local function set_additional()
	local map = M.map
	local k = rc.additional
	local c = rc.keymap()
	map({ "n", "x", "o" }, k.down_fast, "5j")
	map({ "n", "x", "o" }, k.up_fast, "5k")

	-- Add undo break-points
	map("i", ",", ",<C-g>u")
	map("i", ".", ".<C-g>u")
	map("i", ";", ";<C-g>u")

	-- move down
	map("n", "<M-" .. c.down .. ">", "<CMD>m .+1<CR>==")
	map("n", "<M-" .. c.up .. ">", "<CMD>m .-2<CR>==")
	map("x", "<M-" .. c.down .. ">", ":m '>+1<CR>gv=gv")
	map("x", "<M-" .. c.up .. ">", ":m '<-2<CR>gv=gv")
end

M.load = function()
	local k = rc.keymap()
	local map = M.map
	map({ "n", "x" }, k.toggle_case, "~", { silent = true })
	map({ "n", "x" }, k.goto_mark_exact, "`", { silent = true })
	map("n", k.play_macro, "@", {})
	map({ "n", "x", "o" }, k.prev_identifier, "#", {})
	map({ "n", "x", "o" }, k.end_of_line, "$", {})
	map({ "n", "x", "o" }, k.goto_match, "%", {})
	map({ "n", "x", "o" }, k.soft_begin_of_line, "^", {})
	map("n", k.repeat_s, "&", {})
	map({ "n", "x", "o" }, k.next_identifier, "*", {})
	map({ "n", "x", "o" }, k.begin_sentence, "(", {})
	map({ "n", "x", "o" }, k.end_sentence, ")", {})
	map({ "n", "x", "o" }, k.hard_begin_of_line, "0", {})
	map({ "n", "x", "o" }, k.cur_line_bol, "_", {})
	map({ "n", "x", "o" }, k.prev_line_bol, "-")
	map({ "n", "x", "o" }, k.next_line_bol, "+")
	map("n", k.auto_format, "=")
	map("n", k.ex_mode, "Q")
	map("n", k.record_macro, "q")
	map({ "n", "x", "o" }, k.next_word_begin, "w")
	map({ "n", "x", "o" }, k.next_word_begin_include_symbol, "W")
	map({ "n", "x", "o" }, k.next_word_end, "e")
	map({ "n", "x", "o" }, k.next_word_end_include_symbol, "E")
	map({ "n" }, k.replace_word, "r")
	map({ "n" }, k.replace_word_until, "R")
	map({ "n", "x", "o" }, k.until_prev_char, "T")
	map({ "n", "x", "o" }, k.until_next_char, "t")
	map({ "n", "x" }, k.copy_line, "Y")
	map({ "n", "x" }, k.copy, "y")
	map("n", k.undo_line, "U")
	map("n", k.undo, "u")
	map({ "n", "x", "o" }, k.insert_bol, "I")
	map({ "n", "x", "o" }, k.insert, "i")
	map({ "n" }, k.insert_prev_line, "O")
	map("n", k.insert_next_line, "o")
	map("n", k.paste_prev_line, "P")
	map("n", k.paste_next_line, "p")
	map({ "n", "x", "o" }, k.begin_of_paragraph, "{")
	map({ "n", "x", "o" }, k.end_of_paragraph, "}")
	map("n", k.misc_left, "[")
	map("n", k.misc_right, "]")
	map({ "n", "x" }, k.append_eol, "A")
	map({ "n" }, k.append_after_cursor, "a")
	map({ "n", "x" }, k.subst_line, "S")
	map({ "n", "x" }, k.subst_char, "s")
	map({ "n", "x" }, k.del_to_eol, "D")
	map({ "n", "x" }, k.del, "d")
	map({ "n", "x", "o" }, k.find_prev_char, "F")
	map({ "n", "x", "o" }, k.find_next_char, "f")
	map({ "n", "x", "o" }, k.goto_eof, "G")
	map({ "n", "x", "o" }, k.extra_g, "g")
	map({ "n", "x", "o" }, k.top_screen, "H")
	map({ "n", "x", "o" }, k.left, "h")
	map({ "n", "x" }, k.join_lines, "J")
	map({ "n", "x", "o" }, k.down, "v:count == 0 ? 'gj' : 'j'", { expr = true })
	map({ "n", "x" }, k.man_page_identifier, "K")
	map({ "n", "x", "o" }, k.up, "v:count == 0 ? 'gk' : 'k'", { expr = true })
	map({ "n", "x", "o" }, k.bottom_screen, "L")
	map({ "n", "x", "o" }, k.right, "l")
	map({ "n", "x" }, k.repeat_tfTF, ";")
	map({ "n", "x" }, k.register, '"')
	map({ "n", "x", "o" }, k.goto_mark_bol, "'")
	map({ "n", "x", "o" }, k.goto_col_num, "|")
	map("n", k.quit, "Z")
	map({ "n", "x" }, k.extra_z, "z")
	map({ "n", "x" }, k.del_char_backspace, "X")
	map({ "n", "x" }, k.del_char, "x")
	map("n", k.select_lines, "V")
	map("n", k.select_chars, "v")
	map({ "n", "x", "o" }, k.prev_word_begin_include_symbol, "B")
	map({ "n", "x", "o" }, k.prev_word_begin, "b")
	map({ "n", "x", "o" }, k.prev_find, "N")
	map({ "n", "x", "o" }, k.next_find, "n")
	map({ "n", "x", "o" }, k.middle_screen, "M")
	map("n", k.set_mark, "m")
	map("n", k.undent, "<<")
	map("n", k.indent, ">>")
	map({ "n", "x", "o" }, k.reverse_ftFT, ",")
	map("n", k.repeat_cmd, ".")
	-- map({ "n", "x" }, k.find_prev, "?")
	-- map({ "n", "x" }, k.find_next, "/")
	map({ "n", "x" }, k.visual_block, "<C-v>")
	map({ "n", "x" }, k.scroll_down_whole, "<C-f>")
	map({ "n", "x" }, k.scroll_up_whole, "<C-b>")
	map({ "n", "x" }, k.scroll_down_line, "10<C-e>")
	map({ "n", "x" }, k.scroll_up_line, "10<C-y>")
	map({ "n", "x" }, k.scroll_down_half, "<C-d>")
	map({ "n", "x" }, k.scroll_up_half, "<C-u>")
	map("n", k.redo, "<C-r>")
	map("n", k.split_window_horizontal, "<C-w>s")
	map("n", k.split_window_vertical, "<C-w>v")
	map("n", k.toggle_window, "<C-w>w")
	map("n", k.close_window, "<C-w>q")
	map("n", k.swap_window, "<C-w>x")
	map("n", k.switch_left_window, "<C-w>h")
	map("n", k.switch_right_window, "<C-w>l")
	map("n", k.switch_up_window, "<C-w>k")
	map("n", k.switch_down_window, "<C-w>j")
	map("n", k.window_same_width_height, "<C-w>=")
	map("n", k.window_move_left_edge, "<C-w>H")
	map("n", k.window_move_right_edge, "<C-w>L")
	map("n", k.window_move_up_edge, "<C-w>K")
	map("n", k.window_move_down_edge, "<C-w>J")
	map("i", k.insert_backspace, "<BS>")
	map("i", k.insert_del_prev_word, "<C-w>")
	map("i", k.insert_indent, "<C-t>")
	map("i", k.insert_undent, "<C-d>")
	map("i", k.insert_cmp_next, "<C-n>")
	map("i", k.insert_cmp_prev, "<C-p>")
	map("i", k.insert_register, "<C-r>")
	map("i", k.insert_temp_normal, "<C-o>")
	map("x", k.visual_block_corner, "O")
	map("x", k.visual_block_change, "o")
	map("x", k.visual_indent, ">")
	map("x", k.visual_undent, "<")
	map("x", k.visual_yank, "y")
	map("x", k.visual_del, "d")
	map("x", k.visual_caps, "~")
	map("x", k.visual_lowercase, "u")
	map("x", k.visual_uppercase, "U")

	set_others()
	set_additional()

	vim.api.nvim_exec_autocmds("User", {
		pattern = "KEYMAP-SETUP",
		modeline = false,
	})
end

return M

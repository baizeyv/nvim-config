local M = {}

M.setup = function()
    vim.api.nvim_create_autocmd({ "BufReadPost", "BufNewFile", "BufWritePre" }, {
        once = true,
        pattern = "*",
        callback = function()
            vim.api.nvim_exec_autocmds("User", {
                pattern = "FILE-SETUP",
                modeline = false
            })
        end
    })

    local delay_autocmds = vim.fn.argc(-1) == 0
    if not delay_autocmds then
        require("core.autocmd-loader").load()
    end

	require("core.option-loader").load()

    local group = vim.api.nvim_create_augroup("CUSTOM-SETUP-GROUP", { clear = true })
    vim.api.nvim_create_autocmd("User", {
        group = group,
        pattern = "VeryLazy",
        callback = function()
            if delay_autocmds then
                require("core.autocmd-loader").load()
            end
            require("core.keymap-loader").load()

            vim.api.nvim_exec_autocmds("User", {
                pattern = "PLUGIN-SETUP",
                modeline = false
            })
        end
    })

    require("core.plugin-loader").setup()
end

return M

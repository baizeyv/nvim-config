local M = {}

M.root = require("utils.root")
M.lsp = require("utils.lsp")

M.is_win = function()
	return vim.uv.os_uname().sysname:find("Windows") ~= nil
end
return M

local M = {}

local lsputils = require("utils.lsp")

M.spec = { "lsp", { ".git", "lua" }, "cwd" }

M.detectors = {}

--- 获取当前工作目录,返回包含当前工作目录的表
M.detectors.cwd = function()
	return { vim.uv.cwd() }
end

--- 获取与当前缓冲区相关的LSP工作区根目录
M.detectors.lsp = function(buf)
	local bufpath = M.bufpath(buf)
	if not bufpath then
		return {}
	end
	local roots = {} ---@type string[]
	for _, client in pairs(lsputils.get_clients({ bufnr = buf })) do
		-- only check workspace folders, since we're not interested in clients
		-- running in single file mode
		local workspace = client.config.workspace_folders
		for _, ws in pairs(workspace or {}) do
			roots[#roots + 1] = vim.uri_to_fname(ws.uri)
		end
	end
	return vim.tbl_filter(function(path)
		path = M.norm(path)
		return path and bufpath:find(path, 1, true) == 1
	end, roots)
end

--- 检查指定的文件模式,如.git或lua文件夹,是否存在于当前缓冲区路径或其上层路径中
---@param patterns string[] | string
M.detectors.pattern = function(buf, patterns)
	patterns = type(patterns) == "string" and { patterns } or patterns
	local path = M.bufpath(buf) or vim.uv.cwd()
	local pattern = vim.fs.find(function(name)
		for _, p in ipairs(patterns) do
			if name == p then
				return true
			end
			if p:sub(1, 1) == "*" and name:find(vim.pesc(p:sub(2)) .. "$") then
				return true
			end
		end
		return false
	end, { path = path, upward = true })[1]
	return pattern and { vim.fs.dirname(pattern) } or {}
end

M.resolve = function(spec)
	if M.detectors[spec] then
		return M.detectors[spec]
	elseif type(spec) == "function" then
		return spec
	end
	return function(buf)
		return M.detectors.pattern(buf, spec)
	end
end

---@param opts? { buf?:number, spec?, all?:boolean  }
M.detect = function(opts)
	opts = opts or {}
	opts.spec = opts.spec or M.spec
	opts.buf = (opts.buf == nil or opts.buf == 0) and vim.api.nvim_get_current_buf() or opts.buf

	local ret = {}
	for _, spec in ipairs(opts.spec) do
		local paths = M.resolve(spec)(opts.buf)
		paths = paths or {}
		paths = type(paths) == "table" and paths or { paths }
		local roots = {}
		for _, p in ipairs(paths) do
			local pp = M.realpath(p)
			if pp and not vim.tbl_contains(roots, pp) then
				roots[#roots + 1] = pp
			end
		end
		table.sort(roots, function(a, b)
			return #a > #b
		end)
		if #roots > 0 then
			ret[#ret + 1] = { spec = spec, paths = roots }
			if opts.all == false then
				break
			end
		end
	end
	return ret
end

-----------------------------------------------------------

M.cache = {}

vim.api.nvim_create_autocmd({ "LspAttach", "BufWritePost", "DirChanged", "BufEnter" }, {
	group = vim.api.nvim_create_augroup("ROOT_CACHE", { clear = true }),
	callback = function(event)
		M.cache[event.buf] = nil
	end,
})

M.norm = function(path)
	if path:sub(1, 1) == "~" then
		local home = vim.uv.os_homedir() or ""
		if home:sub(-1) == "\\" or home:sub(-1) == "/" then
			home = home:sub(1, -2)
		end
		path = home .. path:sub(2)
	end
	path = path:gsub("\\", "/"):gsub("/+", "/")
	return path:sub(-1) == "/" and path:sub(1, -2) or path
end

--- 将给定路径转换为其真实的绝对路径,并进行规范化处理
M.realpath = function(path)
	if path == "" or path == nil then
		return nil
	end
	path = vim.uv.fs_realpath(path) or path
	return M.norm(path)
end

--- 获取当前工作目录的绝对路径
M.cwd = function()
	return M.realpath(vim.uv.cwd()) or ""
end

--- 获取当前缓冲区的根目录
M.get = function(opts)
	local buf = vim.api.nvim_get_current_buf()
	local ret = M.cache[buf]
	if not ret then
		local roots = M.detect({ all = false })
		ret = roots[1] and roots[1].paths[1] or vim.uv.cwd()
		M.cache[buf] = ret
	end
	if opts and opts.normalize then
		return ret
	end
	return rc.is_win() and ret:gsub("/", "\\") or ret
end

--- 获取当前缓冲区的绝对路径
M.bufpath = function(buf)
	return M.realpath(vim.api.nvim_buf_get_name(assert(buf)))
end

M.git = function()
	local root = M.get()
	local git_root = vim.fs.find(".git", { path = root, upward = true })[1]
	local ret = git_root and vim.fn.fnamemodify(git_root, ":h") or root
	return ret
end

return M

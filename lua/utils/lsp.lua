local M = {}

function M.get_clients(opts)
	local res = {}
	if vim.lsp.get_clients then
		res = vim.lsp.get_clients(opts)
	else
		res = vim.lsp.get_active_clients(opts)
		if opts and opts.method then
			res = vim.tbl_filter(function(client)
				return client.supports_method(opts.method, { bufnr = opts.bufnr })
			end, res)
		end
	end
	return opts and opts.filter and vim.tbl_filter(opts.filter, res) or res
end

function M.on_attach(on_attach_func)
	vim.api.nvim_create_autocmd("LspAttach", {
		callback = function(args)
			local buffer = args.buf
			local client = vim.lsp.get_client_by_id(args.data.client_id)
			on_attach_func(client, buffer)
		end,
	})
end

function M.has_method(buffer, method)
	method = method:find("/") and method or "textDocument/" .. method
	local clients = M.get_clients({ bufnr = buffer })
	for _, client in ipairs(clients) do
		if client.supports_method(method) then
			return true
		end
	end
	return false
end

return M
